﻿using DeveloperFinder.API.Core.Caching;
using DeveloperFinder.API.Core.HttpClients.GitHub;
using DeveloperFinder.API.Core.HttpClients.GitHub.Models.Configuration;
using DeveloperFinder.API.Core.Models.Configuration;
using DeveloperFinder.API.Core.Services;
using DeveloperFinder.API.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using StackExchange.Redis;

namespace DeveloperFinder.API.Extensions
{
    public static class StartupExtensions
    {
        public static IServiceCollection ConfigureSettings(this IServiceCollection services, IConfiguration config)
        {
            services.Configure<DeveloperFinderSettings>(config.GetSection("DeveloperFinderSettings"));
            services.Configure<GitHubSettings>(config.GetSection("GitHubSettings"));

            return services;
        }

        public static IServiceCollection ConfigureServices(this IServiceCollection services)
        {
            services.AddHttpClient<IGitHubHttpClient, GitHubHttpClient>();
            services.AddTransient<IDeveloperFinderService, DeveloperFinderService>();

            return services;
        }

        public static IServiceCollection AddRedisConnection(this IServiceCollection services, string connectionString)
        {
            var config = ConfigurationOptions.Parse(connectionString);
            config.AllowAdmin = true;

            var redisConnection = ConnectionMultiplexer.Connect(config);
            var server = redisConnection.GetServer(connectionString);
            server.FlushAllDatabases();

            services.AddSingleton<IConnectionMultiplexer>(redisConnection);
            services.AddSingleton(redisConnection.GetDatabase());
            services.AddSingleton<IRedisCache, RedisCache>();

            return services;
        }

        public static IServiceCollection AddSwaggerDocumentation(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Developer Finder API", Version = "v1" });
                c.EnableAnnotations();
            });

            return services;
        }

        public static IApplicationBuilder UseSwaggerDocumentation(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1");
                c.DisplayRequestDuration();
                c.DocExpansion(Swashbuckle.AspNetCore.SwaggerUI.DocExpansion.None);
            });

            return app;
        }

        public static IApplicationBuilder UseExceptionHandlerMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionHandlerMiddleware>();
            return app;
        }

    }
}
