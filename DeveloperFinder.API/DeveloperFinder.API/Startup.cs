using DeveloperFinder.API.Core.Models;
using DeveloperFinder.API.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Linq;

namespace DeveloperFinder.API
{
    public class Startup
    {
        public Startup(IWebHostEnvironment env)
        {
            var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            Configuration = configurationBuilder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options => options.EnableEndpointRouting = false)
                .AddJsonOptions(opt => opt.JsonSerializerOptions.IgnoreNullValues = true);

            services.AddControllers();

            //Model Validation
            services.Configure((Action<ApiBehaviorOptions>)(options =>
                options.InvalidModelStateResponseFactory = actionContext =>
                {
                    var response = new ApiResponse
                    {
                        Message = string.Join('\n', actionContext.ModelState
                        .Where(x => x.Value.Errors.Any())
                        .Select(x => x.Value.Errors[0].ErrorMessage))
                    };

                    return new BadRequestObjectResult(response);
                }));

            //Register services in StartupExtensions.cs
            services.ConfigureSettings(Configuration);
            services.AddRedisConnection(Configuration.GetConnectionString("redisCacheConnectionString"));
            services.ConfigureServices();
            services.AddSwaggerDocumentation();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseExceptionHandlerMiddleware();
            app.UseHttpsRedirection();
            app.UseMvc();
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwaggerDocumentation();
        }
    }
}
