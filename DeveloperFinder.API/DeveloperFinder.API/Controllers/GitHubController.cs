﻿using DeveloperFinder.API.Core.Models;
using DeveloperFinder.API.Core.Services;
using DeveloperFinder.API.Models;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Threading.Tasks;

namespace DeveloperFinder.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GitHubController : ControllerBase
    {
        private readonly IDeveloperFinderService _developerFinderService;

        public GitHubController(IDeveloperFinderService developerFinderService)
        {
            _developerFinderService = developerFinderService;
        }

        [HttpPost("getUserInfos")]
        [SwaggerOperation("Takes a maximum of 10 GitHub usernames and returns a list of basic information of the given users retrieved from GitHub.")]
        [SwaggerResponse(200, "A list of basic information of the given users.", typeof(ApiResponse))]
        [SwaggerResponse(500, "An error message.", typeof(ApiResponse))]
        public async Task<IActionResult> GetUserInfos([FromBody] GetUserInfosRequest request)
        {
            var response = await _developerFinderService.GetUserInfos(request.Usernames);

            return StatusCode((int)response.StatusCode, response);
        }
    }
}
