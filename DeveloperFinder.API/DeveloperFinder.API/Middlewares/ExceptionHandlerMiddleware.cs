﻿using DeveloperFinder.API.Core.HttpClients.GitHub.Models;
using DeveloperFinder.API.Core.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;

namespace DeveloperFinder.API.Middlewares
{
    public class ExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionHandlerMiddleware(RequestDelegate next) => _next = next;

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                //Logging can be added here
                Debug.WriteLine($"ERROR - {ex.GetType().Name} - {ex.Message}");
                httpContext.Response.Clear();
                httpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                httpContext.Response.ContentType = "application/json";


                await httpContext.Response.WriteAsync(JsonConvert.SerializeObject(new ApiResponse
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Message = "An error occurred while processing the request."
                }));
            }
        }
    }
}
