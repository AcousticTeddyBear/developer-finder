﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DeveloperFinder.API.Models
{
    public class GetUserInfosRequest
    {
        [JsonProperty("usernames")]
        [Required(ErrorMessage = "The Usernames list should contain at least 1 element and 10 elements at most.")]
        [MinLength(1, ErrorMessage = "The Usernames list should contain at least 1 element and 10 elements at most.")]
        [MaxLength(10, ErrorMessage = "The Usernames list should contain at least 1 element and 10 elements at most.")]
        public List<string> Usernames { get; set; }
    }
}
