﻿using System;

namespace DeveloperFinder.API.Core.HttpClients.GitHub.Models.Exceptions
{
    public class GitHubException : Exception
    {
        public int StatusCode { get; set; }

        public GitHubException(int statusCode) => StatusCode = statusCode;

        public GitHubException(string message, int statusCode) : base(message) => StatusCode = statusCode;

        public GitHubException(string message, Exception innerException, int statusCode) : base(message, innerException) => StatusCode = statusCode;
    }
}
