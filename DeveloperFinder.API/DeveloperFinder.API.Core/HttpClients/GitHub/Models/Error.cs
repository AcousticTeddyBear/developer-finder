﻿using Newtonsoft.Json;

namespace DeveloperFinder.API.Core.HttpClients.GitHub.Models
{
    public class Error
    {
        [JsonProperty("resource")]
        public string Resource { get; set; }

        [JsonProperty("field")]
        public string Field { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }
    }
}
