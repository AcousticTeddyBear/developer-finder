﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace DeveloperFinder.API.Core.HttpClients.GitHub.Models
{
    public class ErrorResponse
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("documentation_url")]
        public string DocumentationUrl { get; set; }

        [JsonProperty("errors")]
        public List<Error> Errors { get; set; }
    }
}
