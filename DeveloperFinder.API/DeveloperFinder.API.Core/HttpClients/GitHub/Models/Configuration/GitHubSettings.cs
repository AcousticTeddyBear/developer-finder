﻿namespace DeveloperFinder.API.Core.HttpClients.GitHub.Models.Configuration
{
    public class GitHubSettings
    {
        public string BaseUrl { get; set; }
    }
}
