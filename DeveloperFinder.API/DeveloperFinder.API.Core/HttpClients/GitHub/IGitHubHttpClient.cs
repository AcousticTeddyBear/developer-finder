﻿using DeveloperFinder.API.Core.HttpClients.GitHub.Models;
using System.Threading.Tasks;

namespace DeveloperFinder.API.Core.HttpClients.GitHub
{
    public interface IGitHubHttpClient
    {
        Task<User> GetUser(string username);
    }
}
