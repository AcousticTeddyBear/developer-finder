﻿using DeveloperFinder.API.Core.HttpClients.GitHub.Models;
using DeveloperFinder.API.Core.HttpClients.GitHub.Models.Configuration;
using DeveloperFinder.API.Core.HttpClients.GitHub.Models.Exceptions;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DeveloperFinder.API.Core.HttpClients.GitHub
{
    public class GitHubHttpClient : IGitHubHttpClient
    {
        private readonly GitHubSettings _gitHubSettings;
        private readonly HttpClient _httpClient;

        public GitHubHttpClient(IOptions<GitHubSettings> gitHubSettings, HttpClient httpClient)
        {
            _gitHubSettings = gitHubSettings.Value;
            _httpClient = httpClient;
            _httpClient.DefaultRequestHeaders.UserAgent.ParseAdd("Developer-Finder-API");
        }

        public Task<User> GetUser(string username)
        {
            if(string.IsNullOrEmpty(username))
            {
                throw new ArgumentNullException(nameof(username));
            }

            return sendRequest<User>($"users/{username}", HttpMethod.Get);
        }

        private async Task<T> sendRequest<T>(string serviceUri, HttpMethod method, object requestBody = null, IDictionary<string, string> query = null)
        {
            var uriBuilder = new UriBuilder(_gitHubSettings.BaseUrl);
            uriBuilder.Path = serviceUri;

            if(query?.Count > 0)
            {
                var queryBuilder = new QueryBuilder(query);
                uriBuilder.Query = queryBuilder.ToString();
            }

            var request = new HttpRequestMessage(method, uriBuilder.ToString());

            if(requestBody != null)
            {
                request.Content = new StringContent(JsonConvert.SerializeObject(requestBody), Encoding.UTF8, "application/json");
            }

            var response = await _httpClient.SendAsync(request);
            var content = await response.Content.ReadAsStringAsync();

            if(!response.IsSuccessStatusCode)
            {
                var errResponse = JsonConvert.DeserializeObject<ErrorResponse>(content);
                var errMessage = errResponse.Message;

                if(errResponse.Errors?.Count > 0)
                {
                    var subErrMessage = string.Join(',', errResponse.Errors.Select(err => $"{err.Field} - {err.Code}"));
                    errMessage = $"{errMessage}: {subErrMessage}";
                }

                throw new GitHubException(errMessage, (int)response.StatusCode);
            }

            return JsonConvert.DeserializeObject<T>(content);
        }
    }
}
