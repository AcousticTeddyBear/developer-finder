﻿using Newtonsoft.Json;
using System.Net;

namespace DeveloperFinder.API.Core.Models
{
    public class ApiResponse
    {
        public ApiResponse()
        {
            StatusCode = HttpStatusCode.OK;
            Message = "Success.";
        }

        [JsonIgnore]
        public HttpStatusCode StatusCode { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("data")]
        public object Data { get; set; }
    }
}
