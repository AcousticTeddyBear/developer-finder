﻿namespace DeveloperFinder.API.Core.Models.Configuration
{
    public class DeveloperFinderSettings
    {
        public double CacheDurationInMinutes { get; set; }
    }
}
