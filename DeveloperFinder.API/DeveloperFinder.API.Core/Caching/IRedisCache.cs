﻿using StackExchange.Redis;
using System;
using System.Threading.Tasks;

namespace DeveloperFinder.API.Core.Caching
{
    public interface IRedisCache
    {
        Task<T> GetAsync<T>(string key, CommandFlags commandFlags = CommandFlags.None);

        Task<bool> SetAsync<T>(string key, T item, TimeSpan? expiry = null, When when = When.Always, CommandFlags commandFlags = CommandFlags.None);

        Task<bool> RemoveAsync(string key, CommandFlags commandFlags = CommandFlags.None);
    }
}
