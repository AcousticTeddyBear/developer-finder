﻿using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Threading.Tasks;

namespace DeveloperFinder.API.Core.Caching
{
    public class RedisCache : IRedisCache
    {
        private readonly IDatabase _cacheDatabase;

        public RedisCache(IDatabase database) => _cacheDatabase = database;

        public async Task<T> GetAsync<T>(string key, CommandFlags commandFlags = CommandFlags.None)
        {
            var redisValue = await _cacheDatabase.StringGetAsync(key, commandFlags);
            return !redisValue.IsNullOrEmpty ? JsonConvert.DeserializeObject<T>(redisValue) : default;
        }

        public Task<bool> RemoveAsync(string key, CommandFlags commandFlags = CommandFlags.None) => _cacheDatabase.KeyDeleteAsync(key, commandFlags);

        public Task<bool> SetAsync<T>(string key, T item, TimeSpan? expiry = null, When when = When.Always, CommandFlags commandFlags = CommandFlags.None) 
            => _cacheDatabase.StringSetAsync(key, JsonConvert.SerializeObject(item), expiry, when, commandFlags);
    }
}
