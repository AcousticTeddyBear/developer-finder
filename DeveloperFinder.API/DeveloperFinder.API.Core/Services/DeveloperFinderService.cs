﻿using DeveloperFinder.API.Core.Caching;
using DeveloperFinder.API.Core.HttpClients.GitHub;
using DeveloperFinder.API.Core.HttpClients.GitHub.Models;
using DeveloperFinder.API.Core.HttpClients.GitHub.Models.Exceptions;
using DeveloperFinder.API.Core.Models;
using DeveloperFinder.API.Core.Models.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DeveloperFinder.API.Core.Services
{
    public class DeveloperFinderService : IDeveloperFinderService
    {
        private readonly DeveloperFinderSettings _developerFinderSettings;
        private readonly IRedisCache _cache;
        private readonly IGitHubHttpClient _gitHubHttpClient;

        public DeveloperFinderService(IOptions<DeveloperFinderSettings> developerFinderSettings, IRedisCache cache, IGitHubHttpClient gitHubHttpClient)
        {
            _developerFinderSettings = developerFinderSettings.Value;
            _cache = cache;
            _gitHubHttpClient = gitHubHttpClient;
        }

        public async Task<ApiResponse> GetUserInfos(List<string> usernames)
        {
            var apiResponse = new ApiResponse();

            if(!usernames?.Any() ?? true)
            {
                apiResponse.StatusCode = HttpStatusCode.BadRequest;
                apiResponse.Message = "The Usernames list should contain at least 1 element and 10 elements at most.";
                return apiResponse;
            }

            var userInfos = new List<User>();

            var tasks = usernames.Select(username => getUserInfo(username, userInfos));
            await Task.WhenAll(tasks);

            return new ApiResponse
            {
                Data = userInfos.OrderBy(x => x.Name)
            };
        }

        private async Task getUserInfo(string username, List<User> userInfos)
        {
            Debug.WriteLine($"GetUserInfo {username} - START");

            try
            {
                if (!string.IsNullOrEmpty(username))
                {
                    var userInfo = await _cache.GetAsync<User>(username);
                    if (userInfo is null)
                    {
                        Debug.WriteLine($"GetUserInfo {username} - Not in cache.");

                        userInfo = await _gitHubHttpClient.GetUser(username);
                        Debug.WriteLine($"GetUserInfo {username} - User retrieved from GitHub. Caching...");

                        await _cache.SetAsync(username, userInfo, TimeSpan.FromMinutes(_developerFinderSettings.CacheDurationInMinutes));
                    }

                    Debug.WriteLine($"GetUserInfo {username} - Add to User Info list.");
                    userInfos.Add(userInfo);
                }
            } catch(GitHubException e)
            {
                //Only possible valid statusCodes of GetUser is 200 (user exists) or 404 (not found)
                //Any other exception for this call is an InternalServerError
                if (e.StatusCode != 404)
                {
                    Debug.WriteLine($"GetUserInfo {username} - ERROR - Status Code: {e.StatusCode} Message: {e.Message}");
                    throw;
                }

                Debug.WriteLine($"GetUserInfo {username} - Not found in GitHub.");
            }

            Debug.WriteLine($"GetUserInfo {username} - END");
        }
    }
}
