﻿using DeveloperFinder.API.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DeveloperFinder.API.Core.Services
{
    public interface IDeveloperFinderService
    {
        Task<ApiResponse> GetUserInfos(List<string> usernames);
    }
}
