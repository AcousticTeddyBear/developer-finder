﻿using DeveloperFinder.API.Core.HttpClients.GitHub;
using DeveloperFinder.API.Core.HttpClients.GitHub.Models.Configuration;
using DeveloperFinder.API.Core.HttpClients.GitHub.Models.Exceptions;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DeveloperFinder.API.Core.Tests.HttpClients.GitHub
{
    [TestFixture]
    public class GitHubHttpClientTests
    {
        private Mock<IOptions<GitHubSettings>> _gitHubSettings;
        private Mock<HttpMessageHandler> _httpMessageHandler;

        private GitHubHttpClient _gitHubHttpClient;

        [SetUp]
        public void Init()
        {
            _gitHubSettings = new Mock<IOptions<GitHubSettings>>();
            _gitHubSettings.SetupGet(x => x.Value)
                .Returns(new GitHubSettings
                {
                    BaseUrl = "http://test.com"
                });

            _httpMessageHandler = new Mock<HttpMessageHandler>();
            var httpClient = new HttpClient(_httpMessageHandler.Object);

            _gitHubHttpClient = new GitHubHttpClient(_gitHubSettings.Object, httpClient);
        }

        [Test]
        public async Task GetUser_ValidUsername_ReturnsExpected()
        {
            //arrange
            var jsonString = TestUtility.GitHubGetUserResponseFile.ReadFromFile();
            var responseMessage = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(jsonString)
            };

            _httpMessageHandler.SetupResponseMessage(responseMessage);

            var username = "egoist";

            //act
            var result = await _gitHubHttpClient.GetUser(username);

            //aasert
            Assert.IsNotNull(result);
        }

        [Test]
        public async Task GetUser_UsernameNotFound_ThrowsException()
        {
            //arrange
            var jsonString = TestUtility.GitHubNotFoundResponseFile.ReadFromFile();
            var responseMessage = new HttpResponseMessage(HttpStatusCode.NotFound)
            {
                Content = new StringContent(jsonString)
            };

            _httpMessageHandler.SetupResponseMessage(responseMessage);

            var username = "iDontExist";

            //act

            //aasert
            Assert.Multiple(() =>
            {
                var exception = Assert.ThrowsAsync<GitHubException>(() => _gitHubHttpClient.GetUser(username));
                Assert.AreEqual(404, exception.StatusCode);
                Assert.AreEqual("Not Found", exception.Message);
            });
        }

        [Test]
        public async Task GetUser_GitHubValidationError_ThrowsException()
        {
            //arrange
            var jsonString = TestUtility.GitHubValidationErrorResponseFile.ReadFromFile();
            var responseMessage = new HttpResponseMessage(HttpStatusCode.UnprocessableEntity)
            {
                Content = new StringContent(jsonString)
            };

            _httpMessageHandler.SetupResponseMessage(responseMessage);

            var username = "iDontExist";

            //act

            //aasert
            Assert.Multiple(() =>
            {
                var exception = Assert.ThrowsAsync<GitHubException>(() => _gitHubHttpClient.GetUser(username));
                Assert.AreEqual(422, exception.StatusCode);
                Assert.AreEqual("Validation Failed: title - missing_field", exception.Message);
            });
        }

        [Test]
        public async Task GetUser_UsernameIsNull_ThrowsException()
        {
            //arrange
            var username = default(string);

            //act

            //aasert
            Assert.Multiple(() =>
            {
                var exception = Assert.ThrowsAsync<ArgumentNullException>(() => _gitHubHttpClient.GetUser(username));
                Assert.AreEqual("Value cannot be null. (Parameter 'username')", exception.Message);
            });
        }

        [Test]
        public async Task GetUser_UsernameIsEmpty_ThrowsException()
        {
            //arrange
            var username = string.Empty;

            //act

            //aasert
            Assert.Multiple(() =>
            {
                var exception = Assert.ThrowsAsync<ArgumentNullException>(() => _gitHubHttpClient.GetUser(username));
                Assert.AreEqual("Value cannot be null. (Parameter 'username')", exception.Message);
            });
        }

    }
}
