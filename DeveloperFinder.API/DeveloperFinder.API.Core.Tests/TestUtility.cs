﻿using Microsoft.Extensions.Caching.Memory;
using Moq;
using Moq.Language.Flow;
using Moq.Protected;
using System;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace DeveloperFinder.API.Core.Tests
{
    public static class TestUtility
    {
        delegate void TryGetValueCallback(object key, out object value);     // needed for Callback
        delegate bool TryGetValueReturns(object key, out object value);      // needed for Returns

        public const string GitHubGetUserResponseFile = "Payloads/GitHubGetUserResponse.json";
        public const string GitHubNotFoundResponseFile = "Payloads/GitHubNotFoundResponse.json";
        public const string GitHubValidationErrorResponseFile = "Payloads/GitHubValidationErrorResponse.json";

        public static IReturnsResult<IMemoryCache> SetupTryGetValue(this Mock<IMemoryCache> mock, object returns) 
            => mock.Setup(x => x.TryGetValue(It.IsAny<object>(), out It.Ref<object>.IsAny))
                .Callback(new TryGetValueCallback((object key, out object value) => value = returns))
                .Returns(new TryGetValueReturns((object key, out object value) =>
                {
                    value = returns;
                    return value != null;
                }));

        public static void SetupResponseMessage(this Mock<HttpMessageHandler> mockHttpMessageHandler, HttpResponseMessage responseMessage)
            => mockHttpMessageHandler
                .Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(responseMessage)
                .Verifiable();

        public static string ReadFromFile(this string fileName)
        {
            var jsonString = string.Empty;
            using (StreamReader r = new StreamReader(Path.Combine(Directory.GetCurrentDirectory(), fileName)))
            {
                jsonString = r.ReadToEnd();
            }

            return jsonString;
        }
    }
}
