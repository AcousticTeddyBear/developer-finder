﻿using DeveloperFinder.API.Core.Caching;
using DeveloperFinder.API.Core.HttpClients.GitHub;
using DeveloperFinder.API.Core.HttpClients.GitHub.Models;
using DeveloperFinder.API.Core.HttpClients.GitHub.Models.Exceptions;
using DeveloperFinder.API.Core.Models.Configuration;
using DeveloperFinder.API.Core.Services;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DeveloperFinder.API.Core.Tests.Services
{
    [TestFixture]
    public class DeveloperFinderServiceTests
    {
        private Mock<IOptions<DeveloperFinderSettings>> _developerFinderSettings;
        private Mock<IRedisCache> _cache;
        private Mock<IGitHubHttpClient> _gitHubHttpClient;

        private DeveloperFinderService _developerFinderService;

        [SetUp]
        public void Setup()
        {
            _developerFinderSettings = new Mock<IOptions<DeveloperFinderSettings>>();
            _developerFinderSettings.SetupGet(x => x.Value)
                .Returns(new DeveloperFinderSettings
                {
                    CacheDurationInMinutes = 2
                });

            _cache = new Mock<IRedisCache>();
            _gitHubHttpClient = new Mock<IGitHubHttpClient>();

            _developerFinderService = new DeveloperFinderService(_developerFinderSettings.Object, _cache.Object, _gitHubHttpClient.Object);
        }

        [Test]
        public async Task GetUserInfos_ValidUsernames_NotYetCached_ReturnsExpected()
        {
            //arrange
            var user1 = new User
            {
                Id = 1,
                Login = "user1",
                Name = "User One",
                Company = "One Company",
                Email = "i.am.1@test.com",
                Bio = "I am user one.",
                Followers = 32,
                PublicRepos = 3
            };

            var user2 = new User
            {
                Id = 2,
                Login = "user2",
                Name = "User Two",
                Company = "Two Incorporated",
                Email = "i.am.2@test.com",
                Bio = "I am user two.",
                Followers = 0,
                PublicRepos = 5
            };

            var user3 = new User
            {
                Id = 3,
                Login = "user3",
                Name = "User Three",
                Company = "Three Limited",
                Email = "i.am.3@test.com",
                Bio = "I am user three.",
                Followers = 3,
                PublicRepos = 1
            };

            _gitHubHttpClient
                .SetupSequence(x => x.GetUser(It.IsAny<string>()))
                .ReturnsAsync(user1)
                .ReturnsAsync(user2)
                .ReturnsAsync(user3);

            var usernames = new List<string>
            {
                user1.Login, user2.Login, user3.Login
            };

            //act
            var result = await _developerFinderService.GetUserInfos(usernames);

            //assert
            Assert.Multiple(() =>
            {
                Assert.IsNotNull(result);
                Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
                Assert.AreEqual("Success.", result.Message);

                var data = result.Data as IEnumerable<User>;
                Assert.IsNotNull(data);
                Assert.IsNotEmpty(data);
                Assert.AreEqual(3, data.Count());

                _cache.Verify(x => x.SetAsync(It.IsAny<string>(), It.IsAny<User>(), It.IsAny<TimeSpan>(), It.IsAny<When>(), It.IsAny<CommandFlags>()), Times.Exactly(3));
            });
        }

        [Test]
        public async Task GetUserInfos_ValidUsernames_User1Cached_ReturnsExpected()
        {
            //arrange
            var user1 = new User
            {
                Id = 1,
                Login = "user1",
                Name = "User One",
                Company = "One Company",
                Email = "i.am.1@test.com",
                Bio = "I am user one.",
                Followers = 32,
                PublicRepos = 3
            };

            var user2 = new User
            {
                Id = 2,
                Login = "user2",
                Name = "User Two",
                Company = "Two Incorporated",
                Email = "i.am.2@test.com",
                Bio = "I am user two.",
                Followers = 0,
                PublicRepos = 5
            };

            var user3 = new User
            {
                Id = 3,
                Login = "user3",
                Name = "User Three",
                Company = "Three Limited",
                Email = "i.am.3@test.com",
                Bio = "I am user three.",
                Followers = 3,
                PublicRepos = 1
            };

            _cache
                .SetupSequence(x => x.GetAsync<User>(It.IsAny<string>(), It.IsAny<CommandFlags>()))
                .ReturnsAsync(user1)
                .ReturnsAsync(default(User))
                .ReturnsAsync(default(User));

            _gitHubHttpClient
                .SetupSequence(x => x.GetUser(It.IsAny<string>()))
                .ReturnsAsync(user2)
                .ReturnsAsync(user3);

            var usernames = new List<string>
            {
                user1.Login, user2.Login, user3.Login
            };

            //act
            var result = await _developerFinderService.GetUserInfos(usernames);

            //assert
            Assert.Multiple(() =>
            {
                Assert.IsNotNull(result);
                Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
                Assert.AreEqual("Success.", result.Message);

                var data = result.Data as IEnumerable<User>;
                Assert.IsNotNull(data);
                Assert.IsNotEmpty(data);
                Assert.AreEqual(3, data.Count());

                _cache.Verify(x => x.SetAsync(It.IsAny<string>(), It.IsAny<User>(), It.IsAny<TimeSpan>(), It.IsAny<When>(), It.IsAny<CommandFlags>()), Times.Exactly(2));
            });
        }

        [Test]
        public async Task GetUserInfos_ValidUsernames_User2DoesNotExist_ReturnsExpected()
        {
            //arrange
            var user1 = new User
            {
                Id = 1,
                Login = "user1",
                Name = "User One",
                Company = "One Company",
                Email = "i.am.1@test.com",
                Bio = "I am user one.",
                Followers = 32,
                PublicRepos = 3
            };

            var user2 = new User
            {
                Id = 2,
                Login = "user2",
                Name = "User Two",
                Company = "Two Incorporated",
                Email = "i.am.2@test.com",
                Bio = "I am user two.",
                Followers = 0,
                PublicRepos = 5
            };

            var user3 = new User
            {
                Id = 3,
                Login = "user3",
                Name = "User Three",
                Company = "Three Limited",
                Email = "i.am.3@test.com",
                Bio = "I am user three.",
                Followers = 3,
                PublicRepos = 1
            };

            _gitHubHttpClient
                .SetupSequence(x => x.GetUser(It.IsAny<string>()))
                .ReturnsAsync(user1)
                .ThrowsAsync(new GitHubException("Not Found.", 404))
                .ReturnsAsync(user3);

            var usernames = new List<string>
            {
                user1.Login, user2.Login, user3.Login
            };

            //act
            var result = await _developerFinderService.GetUserInfos(usernames);

            //assert
            Assert.Multiple(() =>
            {
                Assert.IsNotNull(result);
                Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
                Assert.AreEqual("Success.", result.Message);

                var data = result.Data as IEnumerable<User>;
                Assert.IsNotNull(data);
                Assert.IsNotEmpty(data);
                Assert.AreEqual(2, data.Count());

                _cache.Verify(x => x.SetAsync(It.IsAny<string>(), It.IsAny<User>(), It.IsAny<TimeSpan>(), It.IsAny<When>(), It.IsAny<CommandFlags>()), Times.Exactly(2));
            });
        }

        [Test]
        public async Task GetUserInfos_ValidUsernames_Username2IsNull_ReturnsExpected()
        {
            //arrange
            var user1 = new User
            {
                Id = 1,
                Login = "user1",
                Name = "User One",
                Company = "One Company",
                Email = "i.am.1@test.com",
                Bio = "I am user one.",
                Followers = 32,
                PublicRepos = 3
            };

            var user3 = new User
            {
                Id = 3,
                Login = "user3",
                Name = "User Three",
                Company = "Three Limited",
                Email = "i.am.3@test.com",
                Bio = "I am user three.",
                Followers = 3,
                PublicRepos = 1
            };

            _gitHubHttpClient
                .SetupSequence(x => x.GetUser(It.IsAny<string>()))
                .ReturnsAsync(user1)
                .ReturnsAsync(user3);

            var usernames = new List<string>
            {
                user1.Login, null, user3.Login
            };

            //act
            var result = await _developerFinderService.GetUserInfos(usernames);

            //assert
            Assert.Multiple(() =>
            {
                Assert.IsNotNull(result);
                Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
                Assert.AreEqual("Success.", result.Message);

                var data = result.Data as IEnumerable<User>;
                Assert.IsNotNull(data);
                Assert.IsNotEmpty(data);
                Assert.AreEqual(2, data.Count());

                _cache.Verify(x => x.SetAsync(It.IsAny<string>(), It.IsAny<User>(), It.IsAny<TimeSpan>(), It.IsAny<When>(), It.IsAny<CommandFlags>()), Times.Exactly(2));
            });
        }

        [Test]
        public async Task GetUserInfos_ValidUsernames_Username2IsEmpty_ReturnsExpected()
        {
            //arrange
            var user1 = new User
            {
                Id = 1,
                Login = "user1",
                Name = "User One",
                Company = "One Company",
                Email = "i.am.1@test.com",
                Bio = "I am user one.",
                Followers = 32,
                PublicRepos = 3
            };

            var user3 = new User
            {
                Id = 3,
                Login = "user3",
                Name = "User Three",
                Company = "Three Limited",
                Email = "i.am.3@test.com",
                Bio = "I am user three.",
                Followers = 3,
                PublicRepos = 1
            };

            _gitHubHttpClient
                .SetupSequence(x => x.GetUser(It.IsAny<string>()))
                .ReturnsAsync(user1)
                .ReturnsAsync(user3);

            var usernames = new List<string>
            {
                user1.Login, string.Empty, user3.Login
            };

            //act
            var result = await _developerFinderService.GetUserInfos(usernames);

            //assert
            Assert.Multiple(() =>
            {
                Assert.IsNotNull(result);
                Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
                Assert.AreEqual("Success.", result.Message);

                var data = result.Data as IEnumerable<User>;
                Assert.IsNotNull(data);
                Assert.IsNotEmpty(data);
                Assert.AreEqual(2, data.Count());

                _cache.Verify(x => x.SetAsync(It.IsAny<string>(), It.IsAny<User>(), It.IsAny<TimeSpan>(), It.IsAny<When>(), It.IsAny<CommandFlags>()), Times.Exactly(2));
            });
        }

        [Test]
        public async Task GetUserInfos_ValidUsernames_GitHubThrowsException_ThrowsException()
        {
            //arrange
            var user1 = new User
            {
                Id = 1,
                Login = "user1",
                Name = "User One",
                Company = "One Company",
                Email = "i.am.1@test.com",
                Bio = "I am user one.",
                Followers = 32,
                PublicRepos = 3
            };

            var user2 = new User
            {
                Id = 2,
                Login = "user2",
                Name = "User Two",
                Company = "Two Incorporated",
                Email = "i.am.2@test.com",
                Bio = "I am user two.",
                Followers = 0,
                PublicRepos = 5
            };

            var user3 = new User
            {
                Id = 3,
                Login = "user3",
                Name = "User Three",
                Company = "Three Limited",
                Email = "i.am.3@test.com",
                Bio = "I am user three.",
                Followers = 3,
                PublicRepos = 1
            };

            _gitHubHttpClient
                .SetupSequence(x => x.GetUser(It.IsAny<string>()))
                .ReturnsAsync(user1)
                .ThrowsAsync(new GitHubException("Internal error.", 500))
                .ReturnsAsync(user3);

            var usernames = new List<string>
            {
                user1.Login, user2.Login, user3.Login
            };

            //act

            //assert
            Assert.Multiple(() =>
            {
                var exception = Assert.ThrowsAsync<GitHubException>(() => _developerFinderService.GetUserInfos(usernames));
                Assert.AreEqual(500, exception.StatusCode);
                Assert.AreEqual("Internal error.", exception.Message);
            });
        }

        [Test]
        public async Task GetUserInfos_NullUsernameList_ReturnsBadRequest()
        {
            //arrange
            var usernames = default(List<string>);

            //act
            var result = await _developerFinderService.GetUserInfos(usernames);

            //assert
            Assert.Multiple(() =>
            {
                Assert.IsNotNull(result);
                Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);
                Assert.AreEqual("The Usernames list should contain at least 1 element and 10 elements at most.", result.Message);
            });
        }

        [Test]
        public async Task GetUserInfos_EmptyUsernameList_ReturnsBadRequest()
        {
            //arrange
            var usernames = new List<string>();

            //act
            var result = await _developerFinderService.GetUserInfos(usernames);

            //assert
            Assert.Multiple(() =>
            {
                Assert.IsNotNull(result);
                Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);
                Assert.AreEqual("The Usernames list should contain at least 1 element and 10 elements at most.", result.Message);
            });
        }
    }
}
