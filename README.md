# README

## What's This For?
A Web API that has an endpoint which takes a list of usernames (max of 10) and gets their basic info from GitHub. Infos are cached for 2 minutes and will return data from cache if the user is already in cache.
## Notes

* Built with .NET Core 3.1, unit-testing framework used is NUnit, and IDE used is Visual Studio 2019.
* The cache used is Redis cache. You can configure the redis connection string in appsettings.json.
* If you don't have a Redis cache setup on your machine, I provided a zip file for running a Redis server locally. Just unzip, and run redis-server.exe and a command prompt will launch. Take note of the port used, and go to appsettings.json to modify the connection string with the correct port (e.g. 127.0.0.1:{port}).
* The Base URL of GitHub and the Cache Expiry can all be set in appsettings.json.
* The API endpoint is POST /api/GitHub/getUserInfos. An example of the request body will be added below.
* On startup, a browser will open and will load the Swagger page where you can see all the details of the API. You can also test the endpoint directly on the Swagger page.

## Sample Request Body
```
{
  "usernames": [
    "teddy", "fabpot", "andrew", "taylorotwell", "egoist", "HugoGiraudel", "ornicar", "bebraw", "nelsonic", "jxnblk"
  ]
}
```